package com.bigtester.ate.tcg.runner.screenobject.handler.osfilepicker;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.testng.annotations.Test;

public class WMCTRLTest {
	public String[] execToString(String command) throws Exception {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    CommandLine commandline = CommandLine.parse(command);
	    DefaultExecutor exec = new DefaultExecutor();
	    PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
	    exec.setStreamHandler(streamHandler);
	    exec.execute(commandline);
	    
	    return(outputStream.toString().split("\n"));
	}
  @Test
  public void f() {
	  try {
		  List<String> wins = Arrays.asList(execToString("wmctrl -l"));
		  if (wins.size()==1) {
			  execToString("wmctrl -a " + wins.stream().filter(win->win.contains("\"Open File\"")).collect(Collectors.toList()).get(0));
		  }
		//System.out.println("dialog size: " + wins.stream().filter(win->win.contains("Open File")).collect(Collectors.toList()).size());
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
  }
}
