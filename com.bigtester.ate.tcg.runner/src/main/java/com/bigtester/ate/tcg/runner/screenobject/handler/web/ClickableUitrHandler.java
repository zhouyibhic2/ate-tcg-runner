/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.screenobject.handler.web;

import java.io.IOException;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler.ActionResult;



// TODO: Auto-generated Javadoc
/**
 * This class TextInput defines ....
 * @author Peidong Hu
 *
 */
public class ClickableUitrHandler extends WebElementHandler implements IWebElementHandler {
	
	/**
	 * @param stepRunner
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public ClickableUitrHandler(){
		super( "input");
	}
	

	
	public boolean isTypeable(Element node) {
		
		return !isReadOnly(node) && isVisible(node);
	}
	 
	public UserInputType getApplicableUitrType() {
		 
		return UserInputType.USERINPUT;
	}
	
	public boolean isReadOnly(Element node) {
		Optional<String> readOnly = Optional.ofNullable(node.getAttribute("readonly"));
		if (!readOnly.isPresent()) readOnly = Optional.ofNullable(node.getAttribute("aria-readonly"));
		return !readOnly.isPresent() ? false : readOnly.get().equalsIgnoreCase("readonly") || readOnly.get().equalsIgnoreCase("true") ;
		
	}

	
	public boolean isClickable(Element node) {
		
		return true;
	}

	
	public boolean isVisible(Element node) {
		Optional<String> ateInvisible = Optional.ofNullable(node.getAttribute("ate-invisible"));
		return !ateInvisible.isPresent() ? true : !ateInvisible.get().equalsIgnoreCase("yes");
	}

	
	public IWebElementHandler.ActionResult doAcceptableAction(IMyWebDriver webDriver, String input, Element node) {
		String guid = node.getAttribute(ELEMENT_FIND_ATE_ATTRIBUE_NAME);
		WebElement webE = findWebE(webDriver, guid);
		
		try {
		
			if (webE.isDisplayed() && webE.isEnabled()) {
				
				webE.click();
				
				return IWebElementHandler.ActionResult.SUCCESS;
				
			} else {
				//webE = findWebE(webDriver, guid);
				//webE.click();
				return IWebElementHandler.ActionResult.NOT_VISIBLE_OR_ENABLED;
			}
		
		} catch (ElementNotVisibleException enve){
			return IWebElementHandler.ActionResult.NOT_VISIBLE_EXCEPTION;
		}
		
	}



	 

}
