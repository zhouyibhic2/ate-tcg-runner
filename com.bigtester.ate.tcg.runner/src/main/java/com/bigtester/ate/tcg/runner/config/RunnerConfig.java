/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.config;


import javax.inject.Inject;

import org.bigtester.ate.model.page.elementfind.ElementFindByCss;
import org.bigtester.ate.model.page.elementfind.ElementFindByXpath;
import org.bigtester.ate.model.page.elementfind.IElementFind;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.bigtester.ate.tcg.runner.TebloudRunnerProperties;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.web.DropDownTextInputHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.web.FileInputHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.web.TextAreaHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.web.TextInputHandler;

// TODO: Auto-generated Javadoc
/**
 * The Class WebMvcConfig.
 */
@Configuration
@ComponentScan(basePackages = { "com.bigtester.ate", "org.bigtester.ate" })
@PropertySource("classpath:application.properties")
public class RunnerConfig {

//	@Inject
//	private TebloudRunnerProperties runnerProperties;
//
//	@Bean
//	public TebloudRunnerProperties runnerProperties() {
//		return runnerProperties;
//	}
	@Bean
    public BeanFactoryPostProcessor getPP() {
       PropertyPlaceholderConfigurer configurer = new PropertyPlaceholderConfigurer();
       configurer.setLocations(new Resource[]{new ClassPathResource("application.properties")});
       return configurer;
    }
	@Bean
	public IWebElementHandler textInputHandler() {
		//List<IUiInvisibilityExceptionalPattern> retVal = new ArrayList<IUiInvisibilityExceptionalPattern>();
		//retVal.add(new DivInSpan());
		return new TextInputHandler();
	}

	@Bean
	public IWebElementHandler dropDownTextInputHandler() {
		//List<IUiInvisibilityExceptionalPattern> retVal = new ArrayList<IUiInvisibilityExceptionalPattern>();
		//retVal.add(new DivInSpan());
		return new DropDownTextInputHandler();
	}

	@Bean
	public IWebElementHandler textAreaHandler() {
		//List<IUiInvisibilityExceptionalPattern> retVal = new ArrayList<IUiInvisibilityExceptionalPattern>();
		//retVal.add(new DivInSpan());
		return new TextAreaHandler();
	}
	@Bean
	public IElementFind runnerElementFinderByCss() {
		return new ElementFindByCss();
	}

	@Bean
	public IElementFind runnerElementFinderByXpath() {
		return new ElementFindByXpath();
	}
	@Bean
	public IWebElementHandler fileInputHandler() {
		return new FileInputHandler();
	}

//	/**
//	 * @return the runnerProperties
//	 */
//	public TebloudRunnerProperties getRunnerProperties() {
//		return runnerProperties;
//	}
//
//	/**
//	 * @param runnerProperties the runnerProperties to set
//	 */
//	public void setRunnerProperties(TebloudRunnerProperties runnerProperties) {
//		this.runnerProperties = runnerProperties;
//	}


}
