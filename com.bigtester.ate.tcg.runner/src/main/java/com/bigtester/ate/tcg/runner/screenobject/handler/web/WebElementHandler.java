/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.screenobject.handler.web;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.bigtester.ate.model.page.elementfind.ElementFindByCss;
import org.bigtester.ate.model.page.elementfind.IElementFind;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;

import org.w3c.dom.Node;

// TODO: Auto-generated Javadoc
/**
 * This class InputHandler defines ....
 * @author Peidong Hu
 *
 */
public abstract class WebElementHandler{
	@Autowired
	@Qualifier("runnerElementFinderByXpath")
	private IElementFind xpathElementFinder;
	@Autowired
	@Qualifier("runnerElementFinderByCss")
	private IElementFind cssElementFinder;

	private final Map<String, Set<String>> matchAttributes = new HashMap<String, Set<String>>();
	private final String matchingNodeType;
	private transient Optional<ITestStepRunner> stepRunner = Optional.empty();
	private transient Optional<Element> stepRunnerCoreNode = Optional.empty();
	/**
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws SAXException
	 *
	 */
	public WebElementHandler(String matchingNodeType) {

		this.matchingNodeType = matchingNodeType;



	}
	/**
	 * @return the stepRunner
	 */
	public Optional<ITestStepRunner> getStepRunner() {
		return stepRunner;
	}



	public Map<String, Set<String>> getMatchAttributes() {
		return this.matchAttributes;
	}
//	public WebElement findWebE(IMyWebDriver webDriver, String guid) {
//		WebElement webE;
//		try {
//			webE = this.getElementFinder().doFind(webDriver, "[ate-guid='" + guid +"']");
//		} catch (BrowserUnexpectedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			webE = webDriver.getWebDriverInstance().findElement(By.cssSelector("[ate-guid='" + guid +"']"));
//		}
//		return webE;
//	}
	public WebElement findWebE(IMyWebDriver webDriver, String finderIdentifier) {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid")) {
			finderIdentifier = "[ate-guid='" + finderIdentifier +"']";
		}
		WebElement webE;
		try {
			webE = this.getElementFinder().doFind(webDriver, finderIdentifier);
		} catch (BrowserUnexpectedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			webE = webDriver.getWebDriverInstance().findElement(By.xpath(finderIdentifier));
		}
		return webE;
	}
	/**
	 * Match.
	 *
	 * @param node the node
	 * @return the list
	 *
	 */
	//TODO need to make sure the order of the elements are preserved
	public List<Element> match(Element node) {

		List<Element> retVal = new ArrayList<Element>();

		String nodeType = getMatchingNodeType();
		//LogbackWriter.writeAppInfo("node name: " + node.getTagName());
		Map<String, Set<String>> attrs = getMatchAttributes();
		Set<Entry<String, Set<String>>> matchedAttrs = attrs
				.entrySet()
				.stream()
				.parallel()
				.filter(entry -> node.getAttribute(entry.getKey())!=null && entry.getValue().contains(
						node.getAttribute(entry.getKey())))
				.collect(Collectors.toSet());

		if (!attrs.isEmpty() && matchedAttrs.size() != attrs.entrySet().size()
				|| !nodeType.equalsIgnoreCase(node.getNodeName())) {
			synchronized ( node.getOwnerDocument()) {
				NodeList children = node.getChildNodes();
				for (int index = 0; index < children.getLength(); index++) {
					if (children.item(index)!=null && children.item(index).getNodeType()==Node.ELEMENT_NODE && children.item(index) instanceof Node) {
						List<Element> matchVal = match((Element) children
								.item(index));
						retVal.addAll(matchVal);
					}
				}
			}
		} else {
			retVal.add(node);
		}
		return retVal;
	}
	/**
	 * Match.
	 *
	 * @return the guid of the matched webElement string, or "";
	 */
	public Optional<Element> match(ITestStepRunner runner) {
		this.stepRunner = Optional.of(runner);
		try {
			this.stepRunnerCoreNode = Optional.of(DocumentBuilderFactory
				    .newInstance()
				    .newDocumentBuilder()
				    .parse(new ByteArrayInputStream(runner.getElementCoreHtmlCode().getBytes()))
				    .getDocumentElement());
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
			return Optional.empty();
		}
		Optional<Element> retVal = Optional.empty();
		Element node = this.getStepRunnerCoreNode();


			String nodeType = getMatchingNodeType();

				Map<String, Set<String>> attrs = getMatchAttributes();
				Set<Entry<String, Set<String>>> matchedAttrs = attrs.entrySet().stream().parallel().filter(entry->entry.getValue().contains(node.getAttribute(entry.getKey()))).collect(Collectors.toSet());

				if (!attrs.isEmpty() && matchedAttrs.isEmpty() || !nodeType.equalsIgnoreCase(node.getNodeName())) {
					NodeList children = node.getChildNodes();
					for (int index = 0; index<children.getLength(); index++) {
						if (children.item(index) instanceof Element)
							retVal = singleMatch((Element)children.item(index));
						if (retVal.isPresent()) {
							break;
						}
					}
				} else {
					retVal = Optional.of(node);
				}


		return retVal;
	}
	private Optional<Element> singleMatch(Element node) {
		Optional<Element> retVal = Optional.empty();
		Map<String, Set<String>> attrs = getMatchAttributes();
		Set<Entry<String, Set<String>>> matchedAttrs = attrs.entrySet().stream().parallel().filter(entry->entry.getValue().contains(node.getAttribute(entry.getKey()))).collect(Collectors.toSet());
		String nodeType = getMatchingNodeType();
		if (!attrs.isEmpty() && matchedAttrs.isEmpty() || !nodeType.equalsIgnoreCase(node.getNodeName())) {
			NodeList children = node.getChildNodes();
			for (int index = 0; index<children.getLength(); index++) {
				if (children.item(index) instanceof Element)
					retVal = singleMatch((Element)children.item(index));
				if (retVal.isPresent()) {
					break;
				}
			}
		} else {
			retVal = Optional.of(node);
		}
		return retVal;
	}
	/**
	 * @return the matchingNodeType
	 */
	public String getMatchingNodeType() {
		return matchingNodeType;
	}
	/**
	 * @return the stepRunnerCoreNode
	 */
	public Element getStepRunnerCoreNode() {
		return stepRunnerCoreNode.get();
	}
	/**
	 * @return the elementFinder
	 */
	public IElementFind getElementFinder() {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid"))
			return cssElementFinder;
		else
			return xpathElementFinder;
	}

}
