/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner;


import java.io.IOException;
import java.net.SocketTimeoutException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.bigtester.ate.model.casestep.BaseTestStep;
import org.bigtester.ate.model.casestep.IStepJumpingEnclosedContainer;
import org.bigtester.ate.model.casestep.ITestCase;
import org.bigtester.ate.model.casestep.ITestStep;
import org.bigtester.ate.model.data.exception.RuntimeDataException;
import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.bigtester.ate.model.page.elementfind.ElementFindByCss;
import org.bigtester.ate.model.page.elementfind.ElementFindByXpath;
import org.bigtester.ate.model.page.elementfind.IElementFind;
import org.bigtester.ate.model.page.exception.PageValidationException;
import org.bigtester.ate.model.page.exception.StepExecutionException;
import org.bigtester.ate.systemlogger.LogbackWriter;
import org.eclipse.jdt.annotation.Nullable;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.bigtester.RunnerGlobalUtils;
import com.bigtester.ate.tcg.model.ScreenNamePredictStrategy;
import com.bigtester.ate.tcg.model.domain.HTMLSource;
import com.bigtester.ate.tcg.model.domain.InScreenJumperTrainingRecord;
import com.bigtester.ate.tcg.model.domain.ScreenElementChangeUITR;
import com.bigtester.ate.tcg.model.domain.ScreenJumperElementTrainingRecord;
import com.bigtester.ate.tcg.model.domain.UserInputTrainingRecord;
import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.model.ws.api.ScreenStepsAdvice;
import com.bigtester.ate.tcg.runner.JsInjector;
import com.bigtester.ate.tcg.runner.model.DevToolMessage;
import com.bigtester.ate.tcg.runner.model.ITestStepExecutionContext;
import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IFilePickerDialogHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.tebloud.tcg.config.RunningModeProperties;
import com.bigtester.tebloud.tcg.config.TebloudApplicationProperties;
import com.bigtester.tebloud.tcg.config.RunningModeProperties.RunningMode;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.ScopedProxyMode;

// TODO: Auto-generated Javadoc
/**
 * This class SaveAppliedJobReference defines ....
 * @author Peidong Hu
 *
 */
@Controller
//@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SmartTestExecutionEngine implements ITestStepExecutionContext {
	private final static String LIFE_PORTLET_API_PREFIX = "/tebloud-tcg-liferay-portlet/services";

	@Autowired
	//@Qualifier("runnerElementFinderByCss")
	private List<IElementFind> elementFinders;

	@Inject
	private TebloudRunnerProperties runnerProperties;

	private IMyWebDriver myWebDriver;
	private JsInjector jsInjector;
	private String testCaseName;
	private String username = "test@liferay.com";
	private String password = "taleo123";
	//private String domainName;
	private transient Map<ScreenNamePredictStrategy, ScreenStepsAdvice> currentExecutingScreenStepAdvices = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
	public static long successCount = 0;
	public static long failedCount = 0;
	private final static String TCG_SERVER_IP =  "peidong-3442"; //"52.242.21.98";// "localhost";
	private final static String TCG_SERVER_PORT = "9080";
	HttpHeaders httpHeaders = new HttpHeaders() {
	{
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(
           auth.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        set( "Authorization", authHeader );
     }};

    private RunningModeProperties runningModeProperties;
	//private final String apiResourceUrlPrefix = "http://" + TCG_SERVER_IP + ":" + TCG_SERVER_PORT + TCG_URL_PART1;
	//private final static LinkedMap<String, List<HTMLSource>> executedScreenHTMLSources = new LinkedMap<String, List<HTMLSource>>();
	private final static List<List<ScreenStepsAdvice>> executedStepAdvices = new ArrayList<List<ScreenStepsAdvice>>();

	@Autowired
	private List<IWebElementHandler> webElementHandlers;

	//@Autowired
	private List<IFilePickerDialogHandler> filePickerSystemDialogHandler;

	@PostConstruct
	private void init() {
		this.runningModeProperties = this.queryRunningModeProperties();
	}
	public SmartTestExecutionEngine() {

	}
	public SmartTestExecutionEngine(IMyWebDriver myWebDriver) {
		this.myWebDriver = myWebDriver;
		jsInjector = new JsInjector(getMyWebDriver());
	}
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> getCurrentExecutingScreenStepAdvices() {
		return this.currentExecutingScreenStepAdvices;
	}

	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> setCurrentExecutingScreenStepAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> advices) {
		return this.currentExecutingScreenStepAdvices = advices;

	}

	private String getTCG_TC_URL_PART1(String domainName) {
		return LIFE_PORTLET_API_PREFIX  +"/"+ domainName+"/*/"+testCaseName;
	}

	private String getTCG_SYSTEM_URL_PART1() {
		return LIFE_PORTLET_API_PREFIX;
	}


	private RunningModeProperties queryRunningModeProperties() {
		ClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();;
		RestTemplate restTemplate = new RestTemplate(requestFactory);

		String fooResourceUrl = "http://" + TCG_SERVER_IP + ":" + TCG_SERVER_PORT + getTCG_SYSTEM_URL_PART1() + "/system/properties/runningMode";
		SimpleClientHttpRequestFactory rf =
			    (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
			rf.setReadTimeout(900 * 1000);
			rf.setConnectTimeout(10 * 1000);
			RunningModeProperties retVal = new RunningModeProperties();
		try {

			ResponseEntity<String> jsonReturn = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, new HttpEntity(httpHeaders), String.class);
			//System.out.println(jsonReturn);

			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		    //convert JSON string to Map
		   return mapper.readValue(jsonReturn.getBody(), new TypeReference<RunningModeProperties>(){});

				} catch ( IOException e) {
					LogbackWriter.writeAppInfo("Exception converting json string to map: " );
				     e.printStackTrace();
				     return retVal;
				} catch (RestClientException e) {
				    if (e.getRootCause() instanceof SocketTimeoutException) {
				    	LogbackWriter.writeAppInfo("SocketTimeoutException");
				    } else if (e.getRootCause() instanceof ConnectTimeoutException) {
				    	LogbackWriter.writeAppInfo("ConnectTimeoutException");
				    } else {
				    	LogbackWriter.writeAppInfo("Some other exception:" + e.getLocalizedMessage());
				    }
				    return retVal;
				}

	}

	@Override
	public Optional<ScreenStepsAdvice> isValidStepPath(ScreenStepsAdvice currentExecutingAdvice, ScreenJumperElementTrainingRecord jumper, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> nextExecutingAdvices) {
		Optional<ScreenStepsAdvice> retVal = Optional.empty();
		Map<ScreenNamePredictStrategy, ScreenStepsAdvice> uniqueStepAdvices = nextExecutingAdvices
				.entrySet()
				.stream()
				.parallel()
				.filter(RunnerGlobalUtils.distinctByKey(mapEntry -> mapEntry.getValue()
						.getScreenName()))
				.collect(
						Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		Set<ScreenStepsAdvice> validAdvices = uniqueStepAdvices.values().stream().filter(nextAdvice->{
			return isValidStepPath(currentExecutingAdvice.getDevToolMessage().getDomain(), currentExecutingAdvice.getScreenName(),
					jumper.getInputLabelName(), nextAdvice.getScreenName());
		}).collect(Collectors.toSet());
		if (validAdvices.size()>0) {
			LogbackWriter.writeAppInfo("valid advice detected..");
			retVal = Optional.of(validAdvices.iterator().next());
		}
	   return retVal;
	}
	@Override
	public boolean isValidStepPath(String domainName, String previousScreenName, String jumperName, String nextScreenName) {
		ClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();;
		RestTemplate restTemplate = new RestTemplate(requestFactory);


		Map<String, List<HTMLSource>> req = new HashMap<String, List<HTMLSource>>();

		String fooResourceUrl = "http://" + TCG_SERVER_IP + ":" + TCG_SERVER_PORT + getTCG_TC_URL_PART1(domainName) + "/" + previousScreenName + "/" + jumperName + "/" + nextScreenName;
		HttpEntity<Map<String, List<HTMLSource>>> request = new HttpEntity<>(req, httpHeaders);
		SimpleClientHttpRequestFactory rf =
			    (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
			rf.setReadTimeout(900 * 1000);
			rf.setConnectTimeout(10 * 1000);
		try {

			ResponseEntity<String> jsonReturn = restTemplate.exchange(fooResourceUrl, HttpMethod.GET, new HttpEntity(httpHeaders), String.class);
			//System.out.println(jsonReturn);

			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		    //convert JSON string to Map
		   return mapper.readValue(jsonReturn.getBody(), new TypeReference<Boolean>(){});

				} catch ( IOException e) {
					LogbackWriter.writeAppInfo("Exception converting json boolean result " );
				     e.printStackTrace();
				     return false;
				} catch (RestClientException e) {
				    if (e.getRootCause() instanceof SocketTimeoutException) {
				    	LogbackWriter.writeAppInfo("SocketTimeoutException");
				    } else if (e.getRootCause() instanceof ConnectTimeoutException) {
				    	LogbackWriter.writeAppInfo("ConnectTimeoutException");
				    } else {
				    	LogbackWriter.writeAppInfo("Some other exception:" + e.getLocalizedMessage());
				    }
				    return false;
				}


	}

	@Override
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> getStepAdvice(String testCaseName, DevToolMessage message, ScreenStepsAdvice previousStepAdvice){
		List<HTMLSource> newPageFrameDoms = message.getPages();
		ClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();;
		RestTemplate restTemplate = new RestTemplate(requestFactory);


		Map<String, List<HTMLSource>> req = new HashMap<String, List<HTMLSource>>();
		req.put("newDoms", newPageFrameDoms);
		if (previousStepAdvice != null) {
			req.put("previousDoms", previousStepAdvice.getDevToolMessage().getPages());
		}
		String fooResourceUrl = "http://" + TCG_SERVER_IP + ":" + TCG_SERVER_PORT + getTCG_TC_URL_PART1(message.getDomain()) + "/adviseStepUitrs";
		HttpEntity<Map<String, List<HTMLSource>>> request = new HttpEntity<>(req, httpHeaders);
		SimpleClientHttpRequestFactory rf =
			    (SimpleClientHttpRequestFactory) restTemplate.getRequestFactory();
			rf.setReadTimeout(900 * 1000);
			rf.setConnectTimeout(10 * 1000);
		Map<ScreenNamePredictStrategy, ScreenStepsAdvice> map = new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
		try {

			String jsonReturn = restTemplate.postForObject(fooResourceUrl, request, String.class);
			//System.out.println(jsonReturn);

			ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		    //convert JSON string to Map
		   map = mapper.readValue(jsonReturn, new TypeReference<Map<ScreenNamePredictStrategy, ScreenStepsAdvice>>(){});
		   Map<ScreenNamePredictStrategy, ScreenStepsAdvice> uniqueStepAdvices = map
					.entrySet()
					.stream()
					.parallel()
					.filter(RunnerGlobalUtils.distinctByKey(mapEntry -> mapEntry.getValue()
							.getScreenName()))
					.collect(
							Collectors.toMap(p -> p.getKey(), p -> p.getValue()));
		   uniqueStepAdvices.values().forEach(advice->{
			   advice.setDevToolMessage(message);
		   });
		   return uniqueStepAdvices;
		} catch ( IOException e) {
			LogbackWriter.writeAppInfo("Exception converting json string to map: " );
		     e.printStackTrace();
		     return map;
		} catch (RestClientException e) {
		    if (e.getRootCause() instanceof SocketTimeoutException) {
		    	LogbackWriter.writeAppInfo("SocketTimeoutException");
		    } else if (e.getRootCause() instanceof ConnectTimeoutException) {
		    	LogbackWriter.writeAppInfo("ConnectTimeoutException");
		    } else {
		    	LogbackWriter.writeAppInfo("Some other exception:" + e.getLocalizedMessage());
		    }
		    return map;
		}


	};

	public boolean isSameScreenNamedAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen1advices, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen2advices){
		boolean retVal = true;
		if (screen1advices.size()!=screen2advices.size()) {
			retVal = false;
		} else if (screen1advices.isEmpty() && screen2advices.isEmpty()){
			retVal = false;
		} else if (screen1advices.size()==screen2advices.size()&&screen2advices.size()==1){
			retVal = screen1advices.entrySet().iterator().next().getValue().getScreenName().equalsIgnoreCase(
					screen2advices.entrySet().iterator().next().getValue().getScreenName());
		} else {
			for (Entry<ScreenNamePredictStrategy, ScreenStepsAdvice> screen1entry:screen1advices.entrySet()) {
				//if (screen2advices.get(screen1entry.getKey())==null) continue;
				if (!screen2advices.get(screen1entry.getKey()).getScreenName().equalsIgnoreCase(screen1entry.getValue().getScreenName())) {
					retVal = false;
				}
			}
			for (Entry<ScreenNamePredictStrategy, ScreenStepsAdvice> screen2entry:screen2advices.entrySet()) {
				//if (screen1advices.get(screen2entry.getKey())==null) continue;
				if (!screen1advices.get(screen2entry.getKey()).getScreenName().equalsIgnoreCase(screen2entry.getValue().getScreenName())) {
					retVal = false;
				}
			}
		}
		return retVal;
	}



	public boolean isExactSameScreenAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices1, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices2) {

		return isSameScreenNamedAdvices(stepAdvices1, stepAdvices2) && isSameStepsGuidsScreenAdvices(stepAdvices1, stepAdvices2) && isSameStepsScreenAdvices(stepAdvices1, stepAdvices2);
	}

	public boolean isSameStepsGuidsScreenAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices1, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices2) {

		List<String> allCurrentStepRunnersGuids = stepAdvices1
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.map(ScreenStepsAdvice::getTestStepRunners)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.sorted(Comparator.comparing(runner -> runner.getCoreGuid())).collect(Collectors.toList())
						.stream().map(ITestStepRunner::getCoreGuid).collect(Collectors.toList());

		List<String> allNewStepRunnersGuids = stepAdvices2
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.map(ScreenStepsAdvice::getTestStepRunners)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.sorted(Comparator.comparing(runner -> runner
						.getCoreGuid())).collect(Collectors.toList())
						.stream().map(ITestStepRunner::getCoreGuid).collect(Collectors.toList());

		return allNewStepRunnersGuids.size() == allCurrentStepRunnersGuids.size()
				&& allNewStepRunnersGuids.containsAll(allCurrentStepRunnersGuids);

	}

	public boolean isSameStepsScreenAdvices(Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen1advices, Map<ScreenNamePredictStrategy, ScreenStepsAdvice> screen2advices) {
		List<String> allCurrentStepRunners = screen1advices
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.map(ScreenStepsAdvice::getTestStepRunners)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.sorted(Comparator.comparing(runner -> runner
						.getInputLabelName())).collect(Collectors.toList())
						.stream().map(ITestStepRunner::getInputLabelName).collect(Collectors.toList());

		List<String> allNewStepRunners = screen2advices
				.entrySet()
				.stream()
				.map(Map.Entry::getValue)
				.collect(Collectors.toList())
				.stream()
				.map(ScreenStepsAdvice::getTestStepRunners)
				.collect(Collectors.toList())
				.stream()
				.flatMap(Collection::stream)
				.collect(Collectors.toList())
				.stream()
				.sorted(Comparator.comparing(runner -> runner
						.getInputLabelName())).collect(Collectors.toList())
						.stream().map(ITestStepRunner::getInputLabelName).collect(Collectors.toList());

		return allNewStepRunners.size() == allCurrentStepRunners.size() && allNewStepRunners.containsAll(allCurrentStepRunners);

	}

	private boolean isExecutableInScreenJumper(InScreenJumperTrainingRecord uitr, ScreenStepsAdvice stepAdvice, Iterator<InScreenJumperTrainingRecord> changersItr) {
		boolean retVal = true;

		if (uitr.getPioPredictConfidence() < stepAdvice.getJudgeThreshold()) {
			retVal = false;
		} else {
			Set<InScreenJumperTrainingRecord> sameLabeledUitrs = stepAdvice
					.getInScreenJumperUitrs()
					.stream()
					.parallel()
					.filter(uit -> uit.getInputLabelName().equalsIgnoreCase(
							uitr.getInputLabelName()))
					.collect(Collectors.toSet());

			if (sameLabeledUitrs.size() > 1) {

				for (Iterator<InScreenJumperTrainingRecord> itr = sameLabeledUitrs.iterator(); itr.hasNext();) {
					InScreenJumperTrainingRecord sameLabeledUitr = itr.next();
					if (uitr.getPioPredictConfidence() < sameLabeledUitr
							.getPioPredictConfidence()) {
						retVal = false;
						break;
					} else if (uitr.getPioPredictConfidence() == sameLabeledUitr
							.getPioPredictConfidence()) {
						// Most probably, same functioning nodes
//						if (GlobalUtils.isSameHtmlCodeWithoutAteGuid(
//								uitr.getComparableHtmlCode(),
//								sameLabeledUitr.getComparableHtmlCode())) {
							stepAdvice.getArchivedInScreenJumperUitrs()
									.add(sameLabeledUitr);
							changersItr.remove();
							itr.remove();
							retVal = false;
//							Set<ScreenElementChangeUITR> sameLabeledUitrs2 = stepAdvice
//									.getScreenElementChangeUitrs()
//									.stream()
//									.parallel()
//									.filter(uit -> uit.getInputLabelName()
//											.equalsIgnoreCase(
//													uitr.getInputLabelName()))
//									.collect(Collectors.toSet());
							if (sameLabeledUitrs.size() == 1) {
								break;
							}
						}
//					}
				}
			}
		}
		return retVal;
	}

	public void updateFollowingStepRunnersAfterStepExecution( ) {

	}

	//TODO, now we will simple strategy to judge, which is, if there any user input of the screen in this test case, then all the user inputs should be filled.
	private boolean isExecutableUserInputs(UserInputTrainingRecord uitr, ScreenStepsAdvice stepAdvice, Iterator<UserInputTrainingRecord> userInputUitrsItr) {
		return uitr.isBelongToCurrentTestCase() || !uitr.getTestcases().isEmpty();
	}
	@Override
	public String generateStepNaming(String screenName, String elementLabelName, String actionName,String value, String stepWebElementGuid) {
		if (StringUtils.isEmpty(value))
			return screenName + "==>" + actionName + "==>" + elementLabelName + "==>" + stepWebElementGuid;
		else
			return screenName + "==>" + actionName + "==>" + elementLabelName + "==>" + stepWebElementGuid + "==>" +  value;
	}
	@Override
	public Map<String, String> parseStepNaming(String stepNaming) {
		String [] names = stepNaming.split("==>");
		Map<String, String> nameMap = new ConcurrentHashMap();
		//nameMap.put("stepSequence", names[0]);
		nameMap.put("screenName", names[0]);
		nameMap.put("actionName", names[1]);
		nameMap.put("elementLabelName", names[2]);
		nameMap.put("stepWebElementGuid", names[3]);
		if (names.length==5) {
			nameMap.put("value", names[4]);
		}
		return nameMap;
	}


//	private Map<ScreenNamePredictStrategy, ScreenStepsAdvice> getNewScreenAdvice(String testCaseName, JsInjector injector, ScreenStepsAdvice previousAdvice ) {
//		Optional<DevToolMessage> framePagesMessage = injector.downloadFramePages();
//		DevToolMessage message = framePagesMessage.orElseThrow(()->new IllegalStateException("download frame pages failed"));
//		return getStepAdvice(testCaseName, message, previousAdvice);
//	}


	@Override
	public boolean isInDeedLoopOfScreenJumping() {
		int numberOfRepeatedScreensThrethold = 3;
		boolean retVal = false;
		if (!getExecutedStepAdvices().isEmpty()
				&& getExecutedStepAdvices().get(
						this.getExecutedStepAdvices().size() - 1).size() > numberOfRepeatedScreensThrethold) {
			final ScreenStepsAdvice lastStepAdvice = this.getLastExecutedStepAdvice().get();
			final List<ScreenStepsAdvice> repeatedStepAdvices = new ArrayList<ScreenStepsAdvice>();
			List<ScreenStepsAdvice> lastScreenAdvices = getExecutedStepAdvices().get(
					this.getExecutedStepAdvices().size() - 1);
			for (int index = 1; index < numberOfRepeatedScreensThrethold - 1; index++) {

				repeatedStepAdvices.add(lastScreenAdvices.get(lastScreenAdvices.size() - index - 1));

			}
			Set<String> testRunnerUserInputLabelsInLastStepAdvice = new HashSet<String>();

			lastStepAdvice.getTestStepRunners().stream().parallel().forEach(uitr->{
				testRunnerUserInputLabelsInLastStepAdvice.add(uitr.getInputLabelName());
			});

			final Set<Boolean> differentStepAdvice = new HashSet<Boolean>();
			repeatedStepAdvices.stream().parallel().forEach(stepAdvice->{
				Set<String> testRunnerUserInputLabels = new HashSet<String>();

				stepAdvice.getTestStepRunners().stream().parallel().forEach(uitr->{
					testRunnerUserInputLabels.add(uitr.getInputLabelName());
				});
				if (!(testRunnerUserInputLabelsInLastStepAdvice.containsAll(testRunnerUserInputLabels)
						&& testRunnerUserInputLabels.containsAll(testRunnerUserInputLabelsInLastStepAdvice))){
					differentStepAdvice.add(true);
				}
			});

			retVal = !differentStepAdvice.isEmpty();
		}
		return retVal;
	}

	public boolean isSameScreenStepsAdvice(ScreenStepsAdvice advice1,
			ScreenStepsAdvice advice2) {
		boolean retVal = true;
		Set<String> testRunnerUserInputLabels1 = new HashSet<String>();

			Set<String> testRunnerUserInputLabels2 = new HashSet<String>();

			advice1.getTestStepRunners().stream().parallel().forEach(uitr->{
				testRunnerUserInputLabels1.add(uitr.getInputLabelName());
			});
			advice2.getTestStepRunners().stream().parallel().forEach(uitr->{
				testRunnerUserInputLabels2.add(uitr.getInputLabelName());
			});
			if (!(testRunnerUserInputLabels1.containsAll(testRunnerUserInputLabels2)
					&& testRunnerUserInputLabels2.containsAll(testRunnerUserInputLabels1))){
				retVal = false;
			}


		return retVal;
	}

	public DevToolMessage downloadScreenMessage() {
		Optional<DevToolMessage> framePagesMessage = getJsInjector()
				.downloadFramePages();
		return framePagesMessage
				.orElseThrow(() -> new IllegalStateException("failed download frame page message"));

	}
	@Override
	public Map<ScreenNamePredictStrategy, ScreenStepsAdvice> testScreen(JsInjector injector,
			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices,
			DevToolMessage message) throws Throwable {


		List<ScreenStepsAdvice> allAdvices = new ArrayList<ScreenStepsAdvice>();
		allAdvices.addAll(stepAdvices.values());
		List<ScreenStepsAdvice> sortedAllAdvices = new ArrayList<ScreenStepsAdvice>();
		sortedAllAdvices.addAll(allAdvices);
		int sameScreenNameAdvices = 0;
		int differentNameAdvices = 0;
		if (this.getExecutedStepAdvices().size() > 0 && allAdvices.size()>1) {
			for (int index=0; index<allAdvices.size(); index++) {
				if (this.getLastExecutedStepAdvice()
						.get()
						.getExecutedStepRunner()
						.get(this.getLastExecutedStepAdvice().get()
								.getExecutedStepRunner().size() - 1) instanceof ScreenJumperElementTrainingRecord) {
					if (allAdvices
							.get(index)
							.getScreenName()
							.equalsIgnoreCase(
									this.getExecutedStepAdvices()
											.get(this.getExecutedStepAdvices()
													.size() - 1).get(0)
											.getScreenName())) {
						sameScreenNameAdvices = sameScreenNameAdvices + 1;
						sortedAllAdvices.set(allAdvices.size()
								- sameScreenNameAdvices, allAdvices.get(index));
					} else {
						differentNameAdvices = differentNameAdvices + 1;
						sortedAllAdvices.set(differentNameAdvices - 1,
								allAdvices.get(index));
					}
				} else {
					if (allAdvices
							.get(index)
							.getScreenName()
							.equalsIgnoreCase(
									this.getExecutedStepAdvices()
											.get(this.getExecutedStepAdvices()
													.size() - 1).get(0)
											.getScreenName())) {
						sameScreenNameAdvices = sameScreenNameAdvices + 1;
						sortedAllAdvices.set(sameScreenNameAdvices-1, allAdvices.get(index));
					} else {
						differentNameAdvices = differentNameAdvices + 1;
						sortedAllAdvices.set(allAdvices.size() - differentNameAdvices,
								allAdvices.get(index));
					}
				}
			}
		} else {
			sortedAllAdvices.addAll(allAdvices);
		}

		for (int index = 0; index<sortedAllAdvices.size(); index++){//Entry<ScreenNamePredictStrategy, ScreenStepsAdvice> entry: stepAdvices.entrySet()) {

			ScreenStepsAdvice stepAdvice = sortedAllAdvices.get(index);
			stepAdvice.setDevToolMessage(message);
			LogbackWriter.writeAppInfo("successfully predicted page name is: " + stepAdvice.getScreenName());
			stepAdvice.getTestStepRunners().parallelStream().forEachOrdered(uitr->
			LogbackWriter.writeAppInfo("predictedUitrs is: " + uitr.getUserInputType() + " uitr, "
								+ uitr.getInputLabelName() + ", confidence is: " + uitr.getPioPredictConfidence()
								+ ", probability is: " + uitr.getScreenUitrProbability() + ", htmlCode is:" + uitr.getElementCoreHtmlCode())
					);

			if (stepAdvice.getInScreenJumperUitrs().isEmpty()
					&& stepAdvice.getScreenElementChangeUitrs().isEmpty()
					&& stepAdvice.getScreenJumperUitrs().isEmpty()
					&& !stepAdvice.isLastScreenInTestCase()) {
				failedCount = failedCount + 1;
				LogbackWriter.writeAppError("Failed to parse the click element for this screen! try other screen name prediction strategy.");
				String msg = "Failed screen url is: "
						+ message.getDomainProtocol() + "://"
						+ message.getDomain() + message.getScreenUrl();
				LogbackWriter.writeAppError(msg);
				LogbackWriter.writeAppError(msg);
				continue;
			} else if (stepAdvice.getJudgeThreshold() > stepAdvice
					.getScreenNameConfidence()) {
				failedCount = failedCount + 1;
				LogbackWriter.writeAppError("Failed to get the screen name predicted for this test case!");
				String msg = "Failed screen url is: "
						+ message.getDomainProtocol() + "://"
						+ message.getDomain() + message.getScreenUrl();
				LogbackWriter.writeAppError(msg);

				continue;
			} else {
				this.setCurrentExecutingScreenStepAdvices(stepAdvices);
				successCount = successCount + 1;
				String msg1 = "Success screen url is: "
						+ message.getDomainProtocol() + "://"
						+ message.getDomain() + message.getScreenUrl();
				//System.out.println(msg1);
				LogbackWriter.writeAppInfo(msg1);

				stepAdvice.beforeScreenTestActions(this);

				for (int stepIndex=0; stepIndex<stepAdvice.getTestStepRunners().size(); stepIndex++
						) {
					try {


					ITestStepRunner uitr = stepAdvice.getTestStepRunners().get(stepIndex);
					if (uitr.getUserInputType().equals(UserInputType.SCREENJUMPER) && stepAdvice.getScreenJumperUitrs().size()>1 && !uitr.isExecutable())
						continue;
					if (!stepAdvice.isAlreadyExecutedStepRunner(this, uitr) &&  (uitr.getScreenUitrProbability()>
					10 * 10000 * uitr.getPioPredictConfidence()/2 + 1 || uitr.getUserInputType().equals(UserInputType.SCREENJUMPER) || this.runningModeProperties.getRunningMode().equals(RunningMode.SINGLE_APP_TESTING.toString())
					)) {
						stepAdvice.beforeStepExecutionActions(uitr);
						LogbackWriter.writeAppInfo("Executing uitr step for: " + uitr.getInputLabelName() + " ==HtmlCode==" + uitr.getElementCoreHtmlCode() + " ==");
						if (stepIndex<stepAdvice.getTestStepRunners().size()-1) {
							LogbackWriter.writeAppInfo("Next uitr step for executing is: " + stepAdvice.getTestStepRunners().get(stepIndex+1).getInputLabelName() + " ==HtmlCode==" + stepAdvice.getTestStepRunners().get(stepIndex+1).getElementCoreHtmlCode() + " ==");
						}

						//Before submit this screen, clean up
//						if (uitr instanceof ScreenJumperElementTrainingRecord)
//							injector.executePageUnMarker();
						Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = uitr.executeStep(this, stepAdvice);

						stepAdvice.afterStepExecutionActions(this, uitr);

						//For Demo purpose
//						if (uitr.getUserInputType().equals(UserInputType.SCREENJUMPER)) {
//
//							if (newStepAdvices.size() == 0) {
//								//TODO check if this is an anchor button click. screen view coordinates has moved.
//								newStepAdvices = stepAdvices;
//
//							}
//							newStepAdvices.values().forEach(adv->{
//								adv.setLastScreenInTestCase(true);
//							});
//							getMyWebDriver().saveScreenShot();
//							break;
//						}

						// TODO add case of failed stepAdvise
						if (newStepAdvices.size() == 0) {
							// failed prediction of next screen, we are
							// either
							// 1) we got a new screen that can't be
							// recognized by AI, or
							// 2) we have a false prediction of the this
							// screen jumper.
							// 3) we need to exit the whole execution by
							// throw an exception
							//stepAdvice.afterStepFailedActions(uitr);
							//try different strategy
							//strategyFailed = true;
							break;

						} else if (newStepAdvices.equals(stepAdvices)) {
							//TODO new advices are same as current step advices
							//could mean that we don't have any new screen and new elements come out.
							//It should be a user input step or a failed prediction of element changer or jumper
							if (uitr instanceof UserInputTrainingRecord) {
								stepAdvice.afterStepSuccessActions(uitr);
							}
							//TODO deal with element changer or screen jumper
//							else if (uitr instanceof ){
//								stepAdvice.postUserInputActions(this, uitr);
//							}
							continue;
						} else {
							if (newStepAdvices.size() == 1) {
								//next screen is unique
								//TODO ATTACHRESUME uitr changer bad newStepAdvices
								if (!this.isSameScreenNamedAdvices(stepAdvices, newStepAdvices)) {
									//confirms that this a screen jumper
									stepAdvice.afterStepSuccessActions(uitr);
									stepAdvice.postScreenChangedActions(this, uitr);
									//getANewScreenInElementAction = true;

									//return newStepAdvices
									return newStepAdvices;
									//stepAdvice = newStepAdvice;
									//uniqueStepAdvices = uniqueNewStepAdvices;
									//break;
								} else {
									//this is not a screen jumper, could be element changer
									//???? not sure
									stepAdvice.afterStepExecutionActions(this, uitr);
									stepAdvice.postElementChangedActions(this, newStepAdvices, uitr);
									//as to modify the existing advice step list (which is difficult in Java), we will return a new advices which has removed the executed uitrs (same guids and same labels)
									return newStepAdvices;

								}

							} else {
								// before run next step to decide, we can compare steprunners to have an idea about which
								//strategy is correct? need to prove it.
								//need to run next step to decide if this
								// is a screenJumper or a screen
								// elementChanger
								/*
								 * scenario to handle elementChanger created newStepAdvices
								 */
								if (isSameScreenNamedAdvices(stepAdvices, newStepAdvices)) {
//									if (this.getExecutedStepAdvices().get(this.getExecutedStepAdvices().size()-1).equals(stepAdvice))
//										this.getExecutedStepAdvices().remove(stepAdvice);
									stepAdvice.afterStepExecutionActions(this, uitr);
									stepAdvice.postElementChangedActions(this, newStepAdvices, uitr);
									return newStepAdvices; //continue to next stepRunner and new stepRunner;
								}
								Collection<ScreenStepsAdvice> advices = newStepAdvices.values();
								Iterator<ScreenStepsAdvice> itr2 = advices.iterator();

									this.appendExecutedStepAdvice(stepAdvice);
									Map<ScreenNamePredictStrategy, ScreenStepsAdvice> oneStrategyResult = this.testScreen(injector,
											newStepAdvices, newStepAdvices.values().iterator().next().getDevToolMessage()
											);
									if (oneStrategyResult.isEmpty()) {
										//failed, we will continue to the next strategy, we didn't design to return if >1,
										//but it returned, so something wrong.
										//strategyFailed = true;
										if (this.getLastExecutedStepAdvice().get().equals(stepAdvice))
											this.getExecutedStepAdvices().get(this.getExecutedStepAdvices().size()-1).remove(stepAdvice);
										break; //try next strategy
									} else if (oneStrategyResult.size()==1) {
											//it looks that this path has successfully gone through
										return oneStrategyResult;
									} else {
										if (isSameScreenNamedAdvices(newStepAdvices, oneStrategyResult)) {

											return oneStrategyResult; //continue to next stepRunner and new stepRunner;
										} else {
											//we have tested out a new screen belongs to this test case,
											//but there are more than one strategy predicted screens. need to iterate testScreen function again.
											return this.testScreen(injector,
													oneStrategyResult, oneStrategyResult.values().iterator().next().getDevToolMessage());
										}
									}

							}
						}
					}
				}
//				if (!strategyFailed) {
//					break;
//				}
				catch (Throwable thr) {
					thr.printStackTrace();
					break;
				}
			}
			}

		}
		return new HashMap<ScreenNamePredictStrategy, ScreenStepsAdvice>();
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doStep(@Nullable IStepJumpingEnclosedContainer jumpingContainer) throws StepExecutionException,
			PageValidationException, RuntimeDataException {
		DevToolMessage message = null;
		try {
			//Thread.sleep(3000);
			message = this.downloadScreenMessage();

			Map<ScreenNamePredictStrategy, ScreenStepsAdvice> stepAdvices = getStepAdvice(testCaseName, message, null);
			while (true) {
				Map<ScreenNamePredictStrategy, ScreenStepsAdvice> newStepAdvices = this.testScreen(this.getJsInjector(), stepAdvices, message);
				if (newStepAdvices.size()==0) {
					failedCount = failedCount + 1;
					System.out
							.println("Failed to get the screen name or clicking uitrs predicted for this test case!");
					String msg = "Failed screen url is: "
							+ message.getDomainProtocol() + "//"
							+ message.getDomain() + message.getScreenUrl();
					LogbackWriter.writeAppError(msg);

					// throw new
					// StepExecutionException(ExceptionMessage.MSG_UNCAUGHT_APP_ERRORS,
					// ExceptionErrorCode.UNKNOWN_ERROR, getMyWebDriver(),
					// this.getTestCase());
					getMyWebDriver().getMultiWindowsHandler()
							.closeAllWindowsExceptMainWindow();
					break;
				} else {
					ScreenStepsAdvice stepAdvice = newStepAdvices.values().iterator().next();

					if (stepAdvice.getJudgeThreshold() > stepAdvice
							.getScreenNameConfidence()) {
						failedCount = failedCount + 1;
						LogbackWriter.writeAppError("Failed to get the screen name predicted for this test case!");
						String msg = "Failed screen url is: "
								+ message.getDomainProtocol() + "://"
								+ message.getDomain() + message.getScreenUrl();
						LogbackWriter.writeAppError(msg+" ");

						// throw new
						// StepExecutionException(ExceptionMessage.MSG_UNCAUGHT_APP_ERRORS,
						// ExceptionErrorCode.UNKNOWN_ERROR, getMyWebDriver(),
						// this.getTestCase());
						getMyWebDriver().getMultiWindowsHandler()
								.closeAllWindowsExceptMainWindow();
						break;
					}
					if (stepAdvice.isLastScreenInTestCase()) {
						getMyWebDriver().getMultiWindowsHandler()
								.closeAllWindowsExceptMainWindow();
						break;
					}
//					if (!stepAdvice.getExecutedElementInThisStep().isPresent()) {
//						// nothing has been executed, try different screen name
//						// prediction strategy.
//						failedCount = failedCount + 1;
//						// In many cases, there are some user inputs not filled yet.
//						// screen can't proceed to next one.
//						String msg1 = "Failed screen url is: "
//								+ message.getDomainProtocol() + "://"
//								+ message.getDomain() + message.getScreenUrl();
//						System.out.println(msg1);
//						System.out
//								.println("nothing has been executed on this screen.");
//						LogbackWriter.writeAppInfo(msg1);
//						getMyWebDriver().getMultiWindowsHandler()
//						.closeAllWindowsExceptMainWindow();
//
//						break; // to try next screen Strategy
//					}
					if (isInDeedLoopOfScreenJumping()) {
						failedCount = failedCount + 1;
						// In many cases, there are some user inputs not filled yet.
						// screen can't proceed to next one.
						String msg1 = "Failed screen url is: "
								+ message.getDomainProtocol() + "://"
								+ message.getDomain() + message.getScreenUrl();
						LogbackWriter.writeAppError(msg1);
						LogbackWriter.writeAppError("dead loop of screen actions");

						getMyWebDriver().getMultiWindowsHandler()
						.closeAllWindowsExceptMainWindow();

						break;
					}
					stepAdvices = newStepAdvices;
					message = newStepAdvices.values().iterator().next().getDevToolMessage();
				}
			}
		} catch (Throwable niwde) {
//			StepExecutionException rde = new StepExecutionException(ExceptionMessage.MSG_UNCAUGHT_APP_ERRORS, ExceptionErrorCode.UNKNOWN_ERROR, getMyWebDriver(), this.getTestCase(), niwde);
//			rde.initAteProblemInstance(this).setFatalProblem(true);
//			throw rde;
			if (message != null) {
				String msg1 = "Failed screen url is: " + message.getDomainProtocol() + "://" + message.getDomain() + message.getScreenUrl();
				LogbackWriter.writeAppError(msg1);
				niwde.printStackTrace();
			} else {
				niwde.printStackTrace();
			}
			try {
				getMyWebDriver().getMultiWindowsHandler().closeAllWindowsExceptMainWindow();
			} catch (BrowserUnexpectedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}
	}


	/**
	 * @return the myWebDriver
	 */
	@Override
	public IMyWebDriver getMyWebDriver() {
		return myWebDriver;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getSuccessCount() {
		// TODO Auto-generated method stub
		return successCount;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setSuccessCount(long count) {
		// TODO Auto-generated method stub
		successCount = count;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<List<ScreenStepsAdvice>> getExecutedStepAdvices() {
		// TODO Auto-generated method stub
		return executedStepAdvices;
	}


	/**
	 * @return the jsInjector
	 */
	public JsInjector getJsInjector() {
		return jsInjector;
	}
	/**
	 * @return the webElementHandlers
	 */
	public List<IWebElementHandler> getWebElementHandlers() {
		return webElementHandlers;
	}
	/**
	 * @param webElementHandlers the webElementHandlers to set
	 */
	public void setWebElementHandlers(List<IWebElementHandler> webElementHandlers) {
		this.webElementHandlers = webElementHandlers;
	}
	/**
	 * @param myWebDriver the myWebDriver to set
	 */
	public void setMyWebDriver(IMyWebDriver myWebDriver) {
		this.myWebDriver = myWebDriver;
	}
	/**
	 * @param jsInjector the jsInjector to set
	 */
	public void setJsInjector(JsInjector jsInjector) {
		this.jsInjector = jsInjector;
	}
	/**
	 * @return the filePickerSystemDialogHandler
	 */
	public List<IFilePickerDialogHandler> getFilePickerSystemDialogHandler() {
		return filePickerSystemDialogHandler;
	}
	/**
	 * @param filePickerSystemDialogHandler the filePickerSystemDialogHandler to set
	 */
	public void setFilePickerSystemDialogHandler(
			List<IFilePickerDialogHandler> filePickerSystemDialogHandler) {
		this.filePickerSystemDialogHandler = filePickerSystemDialogHandler;
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ScreenStepsAdvice> getLastExecutedStepAdvice() {
		if (this.getExecutedStepAdvices().isEmpty()) {
			return Optional.empty();
		} else {
			List<ScreenStepsAdvice> lastScreenStepAdvices = this.getExecutedStepAdvices().get(this.getExecutedStepAdvices().size()-1);
			return Optional.of(lastScreenStepAdvices.get(lastScreenStepAdvices.size()-1));
		}
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void appendExecutedStepAdvice(ScreenStepsAdvice newAdvice) {
		if (this.getExecutedStepAdvices().isEmpty() || !this.getExecutedStepAdvices().get(this.getExecutedStepAdvices().size()-1).get(0).getScreenName().equalsIgnoreCase(newAdvice.getScreenName())) {
			ArrayList<ScreenStepsAdvice> advices = new ArrayList<ScreenStepsAdvice>();
			advices.add(newAdvice);
			this.getExecutedStepAdvices().add(advices);
		} else {
			this.getExecutedStepAdvices().get(this.getExecutedStepAdvices().size()-1).add(newAdvice);
		}
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<ScreenStepsAdvice> getPreviousScreenLastStepAdvice(String currentScreenName) {
		if (getExecutedStepAdvices().isEmpty()) {
			return Optional.empty();
		} else {

			if (getExecutedStepAdvices().get(getExecutedStepAdvices().size()-1).get(0).getScreenName().equalsIgnoreCase(currentScreenName)){
				if (getExecutedStepAdvices().size() >= 2) {
					return Optional.of(getExecutedStepAdvices().get(getExecutedStepAdvices().size()-2).get(getExecutedStepAdvices().get(getExecutedStepAdvices().size()-2).size()-1));
				} else {
					return Optional.empty();
				}
			} else {
				return this.getLastExecutedStepAdvice();
			}
		}
	}
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int caculateDomRelativeDistanceByGuid(String domFinderId1, String domFinderId2) {
		return this.getJsInjector().calculateWebElementCommonParentDepth(domFinderId1, domFinderId2)*-1;

	}

	@Override
	public int caculateDomRelativeDistanceByXpath(String xpath1, String xpath2) {
		return this.getJsInjector().calculateWebElementCommonParentDepthByXpath(xpath1, xpath2)*-1;
	}
	@Override
	public int caculateDomRelativeDistanceForFollowingFileInput(String domFinderId) {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid"))
			return caculateDomRelativeDistanceByGuid(domFinderId, "(.//*[@ate-guid='" + domFinderId +"']/following::input[@type='file'])[1]");
		else {
			return this.caculateDomRelativeDistanceByXpath(domFinderId, "(" + domFinderId + "/following::input[@type='file'])[1]");
		}
	}

	@Override
	public int caculateDomRelativeDistanceForPrecedingFileInput(String domFinderId) {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid"))
			return caculateDomRelativeDistanceByGuid(domFinderId, "(.//*[@ate-guid='" + domFinderId +"']/preceding::input[@type='file'])[1]");
		else {
			return this.caculateDomRelativeDistanceByXpath(domFinderId, "(" + domFinderId + "/preceding::input[@type='file'])[1]");
		}
	}
	/**
	 * @return the elementFinder
	 */
	@Override
	public IElementFind getElementFinder(Class<?> elementFinderClass) {
		List<IElementFind> matchedFinders = this.getElementFinders().stream().filter(finder->{
			return elementFinderClass.isInstance(org.bigtester.ate.GlobalUtils.getTargetObject(finder));
		}).collect(Collectors.toList());
		if (matchedFinders.size()>0)
			return matchedFinders.get(0);
		else {
			matchedFinders = this.getElementFinders().stream().filter(finder->{
				return elementFinderClass.isInstance(ElementFindByCss.class);
			}).collect(Collectors.toList());
			return matchedFinders.get(0);
		}
	}

	@Override
	public IElementFind getElementFinder() {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid")) {
			return getElementFinder(ElementFindByCss.class);
		} else {
			return getElementFinder(ElementFindByXpath.class);
		}
	}

	@Override
	public WebElement findElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid")) {
			return getElementFinder(ElementFindByCss.class).doFind(getMyWebDriver(), "[ate-guid='" + finderIdentifier +"']");
		} else {
			return getElementFinder(ElementFindByXpath.class).doFind(getMyWebDriver(), finderIdentifier);
		}
	}

	@Override
	public WebElement findFollowingInputFileWebElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid")) {
			return getElementFinder(ElementFindByCss.class).doFind(getMyWebDriver(), "(.//*[@ate-guid='" + finderIdentifier +"']/following::input[@type='file'])[1]");
		} else {
			return getElementFinder(ElementFindByXpath.class).doFind(getMyWebDriver(), "(" + finderIdentifier + "/following::input[@type='file'])[1]");
		}
	}


	@Override
	public WebElement findPrecedingInputFileWebElement(String finderIdentifier) throws NoSuchElementException, BrowserUnexpectedException {
		if (IWebElementHandler.ELEMENT_FIND_ATE_ATTRIBUE_NAME.equals("ate-guid")) {
			return getElementFinder(ElementFindByCss.class).doFind(getMyWebDriver(), "(.//*[@ate-guid='" + finderIdentifier +"']/preceding::input[@type='file'])[1]");
		} else {
			return getElementFinder(ElementFindByXpath.class).doFind(getMyWebDriver(), "(" + finderIdentifier + "/preceding::input[@type='file'])[1]");
		}
	}
	public List<IElementFind> getElementFinders() {
		return elementFinders;
	}
	/**
	 * @param elementFinder the elementFinder to set
	 */
	public void setElementFinder(List<IElementFind> elementFinders) {
		this.elementFinders = elementFinders;
	}
	/**
	 * @return the testCaseName
	 */
	public String getTestCaseName() {
		return testCaseName;
	}
	/**
	 * @param testCaseName the testCaseName to set
	 */
	public void setTestCaseName(String testCaseName) {
		this.testCaseName = testCaseName;
	}
//	/**
//	 * @return the domainName
//	 */
//	public String getDomainName() {
//		return domainName;
//	}
//	/**
//	 * @param domainName the domainName to set
//	 */
//	public void setDomainName(String domainName) {
//		this.domainName = domainName;
//	}
	/**
	 * @return the runnerProperties
	 */
	public TebloudRunnerProperties getRunnerProperties() {
		return runnerProperties;
	}
	/**
	 * @param runnerProperties the runnerProperties to set
	 */
	public void setRunnerProperties(TebloudRunnerProperties runnerProperties) {
		this.runnerProperties = runnerProperties;
	}

}
