/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.screenobject.handler.web;

import java.io.IOException;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.bigtester.ate.tcg.model.domain.WebElementTrainingRecord.UserInputType;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;

// TODO: Auto-generated Javadoc
/**
 * This class TextInput defines ....
 * @author Peidong Hu
 *
 */
public class TextAreaHandler extends WebElementHandler implements IWebElementHandler{
	
	/**
	 * @param stepRunner
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	public TextAreaHandler(){
		super( "textarea");
	}
	
	
	
	public boolean isTypeable(Element node) {
		
		return !isReadOnly(node) && isVisible(node);
	}

	
	public boolean isReadOnly(Element node) {
		Optional<String> readOnly = Optional.ofNullable(node.getAttribute("readonly"));
		if (!readOnly.isPresent()) readOnly = Optional.ofNullable(node.getAttribute("aria-readonly"));
		return !readOnly.isPresent() ? false : readOnly.get().equalsIgnoreCase("readonly") || readOnly.get().equalsIgnoreCase("true") ;
		
	}

	
	public boolean isClickable(Element node) {
		
		return true;
	}

	
	public boolean isVisible(Element node) {
		Optional<String> ateInvisible = Optional.ofNullable(node.getAttribute("ate-invisible"));
		return !ateInvisible.isPresent() ? true : !ateInvisible.get().equalsIgnoreCase("yes");
	}

	public IWebElementHandler.ActionResult doAcceptableAction(IMyWebDriver webDriver, String input, Element node) {
		String guid = node.getAttribute(ELEMENT_FIND_ATE_ATTRIBUE_NAME);
		WebElement webE = this.findWebE(webDriver, guid);
		//WebElement webE = webDriver.findElement(By.cssSelector("[ate-guid='" + guid +"']"));
		
		if (isTypeable(node)) {
			if (webE.isDisplayed() && webE.isEnabled()) {
				if (webE.getAttribute("value").equalsIgnoreCase(input))
					return IWebElementHandler.ActionResult.SUCCESS;
				webE.sendKeys(Keys.chord(Keys.CONTROL, "a"),input);
				webE = this.findWebE(webDriver, guid);
				if (webE.getAttribute("value").equalsIgnoreCase(input))
					return IWebElementHandler.ActionResult.SUCCESS;
				else
					return IWebElementHandler.ActionResult.VALUE_NOT_TYPED_IN_CORRECTLY;
			} else if (isClickable(node)){
				//webE.click();
				return IWebElementHandler.ActionResult.NOT_VISIBLE_OR_ENABLED;
			}
		} else if (isClickable(node)){
			//webE.click();
			return IWebElementHandler.ActionResult.NOT_TYPABLE;
		}
		return IWebElementHandler.ActionResult.FAILED_FOR_UNKNOWN_REASON;
	}



	/**
	 * {@inheritDoc}
	 */
	@Override
	public UserInputType getApplicableUitrType() {
		 
		return UserInputType.USERINPUT;
	}

}
