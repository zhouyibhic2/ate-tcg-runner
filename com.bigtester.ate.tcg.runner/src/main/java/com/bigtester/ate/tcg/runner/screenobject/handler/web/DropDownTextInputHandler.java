/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2016, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.ate.tcg.runner.screenobject.handler.web;


import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.bigtester.ate.model.page.atewebdriver.IMyWebDriver;
import org.bigtester.ate.model.page.atewebdriver.exception.BrowserUnexpectedException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.bigtester.ate.tcg.runner.model.ITestStepRunner;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler;
import com.bigtester.ate.tcg.runner.screenobject.handler.IWebElementHandler.ActionResult;
import com.google.common.collect.Sets;

// TODO: Auto-generated Javadoc
/**
 * This class TextInputHandler defines ....
 * @author Peidong Hu
 *
 */
public class DropDownTextInputHandler extends TextInputHandler implements IWebElementHandler{
	public static final String[] DropDownInputValues = {"▼ ", "▼"};
	
	public static final String[] DropDownInputReadonlyValues = {"readonly"};
	
	/**
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * 
	 */
	public DropDownTextInputHandler(){
		super();
		this.getMatchAttributes().put("value", Sets.newHashSet(DropDownInputValues));
		this.getMatchAttributes().put("readonly", Sets.newHashSet(DropDownInputReadonlyValues));
	}
	@Override
	public IWebElementHandler.ActionResult doAcceptableAction(IMyWebDriver webDriver, String input, Element node) {
		String guid = node.getAttribute(ELEMENT_FIND_ATE_ATTRIBUE_NAME);
		WebElement webE = this.findWebE(webDriver, guid);
		//WebElement webE = webDriver.findElement(By.cssSelector("[ate-guid='" + guid +"']"));
		
		webE.click();
		return IWebElementHandler.ActionResult.NOT_TYPABLE;
		
	}
}
