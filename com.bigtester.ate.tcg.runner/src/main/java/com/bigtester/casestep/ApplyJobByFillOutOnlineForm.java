/*******************************************************************************
 * ATE, Automation Test Engine
 *
 * Copyright 2015, Montreal PROT, or individual contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Montreal PROT.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.bigtester.casestep;


import java.net.URI;
import java.net.URISyntaxException;

import org.bigtester.ate.model.casestep.AbstractBaseJavaCodedStep;
import org.bigtester.ate.model.casestep.BaseTestStep;
import org.bigtester.ate.model.casestep.HomeStep;
import org.bigtester.ate.model.casestep.IJavaCodedStep;
import org.bigtester.ate.model.casestep.IStepJumpingEnclosedContainer;
import org.bigtester.ate.model.casestep.ITestCase;
import org.bigtester.ate.model.data.exception.RuntimeDataException;
import org.bigtester.ate.model.page.exception.PageValidationException;
import org.bigtester.ate.model.page.exception.StepExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import com.bigtester.ate.tcg.runner.JsInjector;
import com.bigtester.ate.tcg.runner.SmartTestExecutionEngine;

// TODO: Auto-generated Javadoc
/**
 * This class SaveAppliedJobReference defines ....
 * @author Peidong Hu
 *
 */
public class ApplyJobByFillOutOnlineForm extends AbstractBaseJavaCodedStep
		implements IJavaCodedStep {

	@Autowired
	SmartTestExecutionEngine engine;
	private String parseTestCaseName(IStepJumpingEnclosedContainer jumpingContainer) {
		String testCaseName = "";
		if (jumpingContainer instanceof ITestCase) {
			testCaseName = ((ITestCase) jumpingContainer).getTestCaseName();
		} else if(jumpingContainer instanceof BaseTestStep) {
			testCaseName = ((BaseTestStep)jumpingContainer).getTestCase().getTestCaseName();
		}
		return testCaseName;
	}



	private String parseDomainName(IStepJumpingEnclosedContainer jumpingContainer) throws URISyntaxException {
		String url = "";
		if (jumpingContainer instanceof ITestCase) {
			url = ((HomeStep)((ITestCase) jumpingContainer).getTestStepList().get(0)).getHomePage().getHomeUrl();
		} else if(jumpingContainer instanceof BaseTestStep) {
			url = ((HomeStep)((BaseTestStep)jumpingContainer).getTestCase().getTestStepList().get(0)).getHomePage().getHomeUrl();
		}
		URI uri = new URI(url);
		return (uri.getPort()==-1) ? uri.getHost() : uri.getHost() + ":" + uri.getPort();
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public void doStep(IStepJumpingEnclosedContainer jumpingContainer)
			throws StepExecutionException, PageValidationException,
			RuntimeDataException {
		engine.setMyWebDriver(getMyWebDriver());
		engine.setJsInjector(new JsInjector(getMyWebDriver()));
		engine.setTestCaseName(this.parseTestCaseName(jumpingContainer));
//		try {
//			engine.setDomainName(this.parseDomainName(jumpingContainer));
//		} catch (URISyntaxException e) {
//			throw new RuntimeDataException("home url domain name + host port parsing error", "Code1001", e);
//		}
		engine.doStep(jumpingContainer);
		// TODO Auto-generated method stub

	}

}
